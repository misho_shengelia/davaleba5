package com.example.davaleba5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        loginbutton.setOnClickListener {
            if (loginedit.text.toString().isNotEmpty() && passwordedit.text.toString()
                    .isNotEmpty()
            ){
                profile()
            }
        }
    }

    private fun profile(){
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
    }

}